import XCTest

import QuantumUSB_BluetoothBeaconTests

var tests = [XCTestCaseEntry]()
tests += QuantumUSB_BluetoothBeaconTests.allTests()
XCTMain(tests)
