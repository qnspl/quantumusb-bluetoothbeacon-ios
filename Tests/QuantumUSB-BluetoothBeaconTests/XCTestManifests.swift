import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(QuantumUSB_BluetoothBeaconTests.allTests),
    ]
}
#endif
