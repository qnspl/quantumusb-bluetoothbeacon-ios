import XCTest
@testable import QuantumUSB_BluetoothBeacon

final class QuantumUSB_BluetoothBeaconTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(QuantumUSB_BluetoothBeacon().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
