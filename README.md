# QuantumUSB-BluetoothBeacon

QuantumUSB-BluetoothBeacon SDK created to scan Beacons

## Requirements

- iOS 12.0+
- Xcode 11


## Integration

#### Swift Package Manager

When using Xcode 11 or later, you can install `QuantumUSB-BluetoothBeacon` by going to your Project settings > `Swift Packages` and add the repository by providing the Gitlab URL: 'https://gitlab.com/qnspl/quantumusb-bluetoothbeacon-ios.git'. Alternatively, you can go to `File` > `Swift Packages` > `Add Package Dependencies...`

```swift
// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "YOUR_PROJECT_NAME",
    dependencies: [
        .package(url: "https://gitlab.com/qnspl/quantumusb-bluetoothbeacon-ios.git"),
    ]
)
```


## Example

Retrieve QuantumUSB_BluetoothBeacon

import QuantumUSB_BluetoothBeacon


```Swift
var Quantum_Beacon:QuantumUSB_BluetoothBeacon()
```

Configure QuantumUSB_BluetoothBeaconDelegate
```Swift
Quantum_Beacon.delegate = self

```

Start scan
```swift
Quantum_Beacon.Start_Scanning(AppID: "your APP ID")
```

Receive listener calls
```swift
QuantumUSB_BluetoothBeaconDelegate
```
```swift
func didReceivedAdvertiseDetails(AdvertiseData: NSDictionary)
{
  print(AdvertiseData)
}

func didClickedOnAdvertise(AdvertiseData: NSDictionary)
{
  print(AdvertiseData)
}
```

Stop scan
```swift
Quantum_Beacon.stop_Scan()
```

Extras
```swift
//turn on Notification (false by default)
Quantum_Beacon.is_enable_notification = true
```


Note
```
- Make sure you give Bluetooth and Location Permission(Location Permission to 'Alwase' to run SDK in background also).
- if you want to run this SDK in background, you have to call this all from 'AppDelegate'
- you need to add NSLocationAlwaysUsageDescription in info.plist to run app in background as-well.
- you need to add NSLocationUsageDescription, NSLocationWhenInUseUsageDescription, NSLocationAlwaysAndWhenInUseUsageDescription in info.plist.
- you need to add NSBluetoothAlwaysUsageDescription in info.plist.
```


=======
# QuantumUSB-BluetoothBeacon




