import UIKit
import WebKit
import CoreLocation
import CoreBluetooth
import MapKit


/// Implement this protocol to receive Data/notifications.
@objc public protocol QuantumUSB_BluetoothBeaconDelegate {
    
    @objc optional func didClickedOnAdvertise(AdvertiseData: NSDictionary)
    
    @objc optional func didReceivedAdvertiseDetails(AdvertiseData: NSDictionary)
}


open class QuantumUSB_BluetoothBeacon: NSObject, CLLocationManagerDelegate,UNUserNotificationCenterDelegate,CBCentralManagerDelegate
{
    var window: UIWindow?
    
    open var delegate: QuantumUSB_BluetoothBeaconDelegate?
    
    fileprivate let regionIdentifier = "QuantumUSB_BluetoothBeaconScanner"
    
    // CLLocationManager that will listen and react to Beacons.
    var Location_Manager: CLLocationManager = CLLocationManager()
    var CentralManager:CBCentralManager!
    
    var Arr_Beacon: Array<NSDictionary> = []
    
    open var is_enable_notification = false
    
    var Str_APP_ID =  ""
    
    var timer_Get_BeaconList = Timer()
    
    var Dict_Beacon_Advertise_Data = NSDictionary()
    
    
    open func Start_Scanning(AppID:String)
    {
        Str_APP_ID = AppID
        self.Get_Beacon_List()
    }
    
    @objc open func Get_Beacon_List()
    {
        let headers = [
            "content-type": "application/json",
        ]
        
        let parameters = ["api_unique_key":Str_APP_ID] as [String : Any]
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        let requestURL = "https://rudder.dev.qntmnet.com/wsmp/beacon-api/get-beacon-list"
        
        let request = NSMutableURLRequest(url: NSURL(string:requestURL)! as URL,cachePolicy:.useProtocolCachePolicy,timeoutInterval:45.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if error == nil {
                if data != nil {
                    let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                    if json != nil
                    {
                        let dataDict : NSDictionary = json as! NSDictionary
                        
                        if dataDict["responseCode"] as! String == "200"
                        {
                            let tmp_arr = dataDict["responseData"] as! Array<NSDictionary>
                            
                            if self.Arr_Beacon != tmp_arr
                            {
                                self.Arr_Beacon = []
                                self.Arr_Beacon = dataDict["responseData"] as! Array<NSDictionary>
                                
                                DispatchQueue.main.async {
                                    self.Ask_For_Location_Permission()
                                }
                            }
                        }
                        else
                        {
                            self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: dataDict["responseMsg"] as! String)
                        }
                    }
                    else
                    {
                        self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: "Somthing went Wrong, try after Sometime")
                    }
                }
                else
                {
                    self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: "Somthing went Wrong, try after Sometime")
                    
                }
            }
            else
            {
                self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: "Somthing went Wrong, try after Sometime")
            }
        });dataTask.resume()
    }
  
    
    func Open_Alert(Title:String, Message: String)
    {
        let Alert = UIAlertController(title: Title, message: Message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
        }
        
        Alert.addAction(okAction)
        
        if let topVC = self.getTopViewController()
        {
            DispatchQueue.main.async {
                topVC.navigationController!.present(Alert, animated: true, completion: nil)
            }
        }
    }
    
    
    func Ask_For_Location_Permission()
    {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            center.delegate = self
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound]) {(accepted, error) in
                if !accepted {
                }
            }
        }
        else
        {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
        Location_Manager = CLLocationManager()
        Location_Manager.delegate = self
        
        DispatchQueue.global().async {
            if (CLLocationManager.locationServicesEnabled())
            {
                let status: CLAuthorizationStatus
                
                if #available(iOS 14, *)
                {
                    status = self.Location_Manager.authorizationStatus
                }
                else
                {
                    status = CLLocationManager.authorizationStatus()
                }
                
                if status == .notDetermined
                {
                    self.Location_Manager.requestAlwaysAuthorization()
                }
                else if status == .denied || status == .restricted
                {
                    self.Location_Manager.requestAlwaysAuthorization()
                    
                }
                else if status == .authorizedAlways || status == .authorizedWhenInUse
                {
                    if (self.Location_Manager.location?.coordinate) != nil
                    {
                        // Check for Beeacon
                        self.CentralManager = CBCentralManager()
                        self.CentralManager.delegate = self
                        
                        let seconds = 2.0
                        DispatchQueue.main.asyncAfter(deadline: .now() + seconds)
                        {
                            self.timer_Get_BeaconList.invalidate()
                            self.Start_ScanningFor_BEACON()
                        }
                    }
                    else
                    {
                        self.Open_location_Alert()
                        
                    }
                }
            }
            else
            {
                self.Open_location_Alert()
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if status == .authorizedAlways || status == .authorizedAlways
        {
            self.timer_Get_BeaconList.invalidate()
            self.Start_ScanningFor_BEACON()
        }
        else if status == .denied
        {
            self.Open_location_Alert()
        }
    }
    
    func Open_location_Alert()
    {
        let Alert_LocationPermission = UIAlertController(title: "Enable Loaction Service", message: "Enable Location To use QuantumUSB BluetoothBeacon", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
            else
            {
                UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
            }
        }
        
        Alert_LocationPermission.addAction(okAction)
        if let topVC = self.getTopViewController()
        {
            DispatchQueue.main.async {
                topVC.navigationController!.present(Alert_LocationPermission, animated: true, completion: nil)
            }
        }
    }
    
    
    
    
   
    func Start_ScanningFor_BEACON()
    {
        if #available(iOS 10.0, *) {
            timer_Get_BeaconList = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { _ in
                self.Get_Beacon_List()
            })
        } else {
            // Fallback on earlier versions
            
            timer_Get_BeaconList = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.Get_Beacon_List), userInfo: nil, repeats: true)

        }
        
        Location_Manager.delegate = self
        Location_Manager.activityType = .automotiveNavigation
        Location_Manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        Location_Manager.distanceFilter = 10.0
        Location_Manager.requestAlwaysAuthorization()
        
        if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
            
            //  Can we detect the distance of a beacon?
            if CLLocationManager.isRangingAvailable() {
                
                for i in 0..<Arr_Beacon.count
                {
                    let Beacon_UUID = String(format: "%@", Arr_Beacon[i]["uuid"] as! String)
                    let Beacon_Identifier = String(format: "%@", Arr_Beacon[i]["location"] as! String)
                    let Beacon_Name = String(format: "%@", Arr_Beacon[i]["name"] as! String)
                    startScanning(uuid: UUID(uuidString: Beacon_UUID)!, major: 1, minor: 1, identifier: Beacon_Identifier, name: Beacon_Name)
                }
            }
        }
    }
    
    open func Stop_Scanning()
    {
        for i in 0..<Arr_Beacon.count
        {
            let Beacon_UUID = UUID(uuidString:String(format: "%@", Arr_Beacon[i]["uuid"] as! String))
            let identifier = String(format: "%@", Arr_Beacon[i]["location"] as! String)
            
            if #available(iOS 13.0, *)
            {
                let constraint = CLBeaconIdentityConstraint(uuid: Beacon_UUID!)
                let region = CLBeaconRegion(beaconIdentityConstraint: constraint, identifier: identifier)
                stopListening(region)
            }
            else
            {
                let region = CLBeaconRegion(proximityUUID: Beacon_UUID!, identifier: identifier)
                stopListening(region)
            }
        }
    }
    
    fileprivate func stopListening(_ region: CLBeaconRegion)
    {
        Location_Manager.stopRangingBeacons(in: region)
        Location_Manager.stopMonitoring(for: region)
    }
    
    func startScanning(uuid: UUID, major: UInt16, minor: UInt16, identifier: String, name: String)
    {
        if #available(iOS 13.0, *)
        {
            let constraint = CLBeaconIdentityConstraint(uuid: uuid)
            let region = CLBeaconRegion(beaconIdentityConstraint: constraint, identifier: identifier)
            region.notifyOnEntry = true
            region.notifyOnExit = true
            //            region.notifyEntryStateOnDisplay = true
            Location_Manager.startMonitoring(for: region)
            Location_Manager.startRangingBeacons(satisfying: constraint)
            Location_Manager.startUpdatingLocation()
        }
        else
        {
            // Fallback on earlier versions
            let beaconRegion = CLBeaconRegion(proximityUUID: uuid, identifier: identifier)
            beaconRegion.notifyOnEntry = true;
            beaconRegion.notifyOnExit = true;
            //            beaconRegion.notifyEntryStateOnDisplay = true;
            Location_Manager.startMonitoring(for: beaconRegion)
            Location_Manager.startRangingBeacons(in: beaconRegion)
            Location_Manager.startUpdatingLocation()
        }
    }
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        if central.state == .poweredOff
        {
            self.Open_Alert(Title: "Bluetooth Error..!!", Message: "Please turn-on Bluetooth to use QuantumUSB BluetoothBeacon")
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        Location_Manager.requestState(for: region);
    }
    
    public func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion)
    {
        if state == .inside
        {
            let beaconRegion = region as! CLBeaconRegion
            self.Get_Beacon_Advertise_Data(Beacon_UUID: String(format: "%@", beaconRegion.proximityUUID as CVarArg), is_from_Notification: false)
        }
    }
    
    func Get_Beacon_Advertise_Data(Beacon_UUID:String, is_from_Notification:Bool)
    {
        let headers = [
            "content-type": "application/json",
        ]
        
        let parameters = ["api_unique_key":Str_APP_ID, "uuid":Beacon_UUID] as [String : Any]
        
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        let requestURL = "https://rudder.dev.qntmnet.com/wsmp/beacon-api/get-campaign-detail"
        
        let request = NSMutableURLRequest(url: NSURL(string:requestURL)! as URL,cachePolicy:.useProtocolCachePolicy,timeoutInterval:45.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if error == nil {
                if data != nil {
                    let json = try? JSONSerialization.jsonObject(with: data!, options: [])
                    if json != nil
                    {
                        let dataDict : NSDictionary = json as! NSDictionary
                        
                        self.Dict_Beacon_Advertise_Data = NSDictionary()
                        
                        if dataDict["responseCode"] as! String == "200"
                        {
                            self.Dict_Beacon_Advertise_Data = dataDict["responseData"] as! NSDictionary
                            
                            
                            self.delegate?.didReceivedAdvertiseDetails?(AdvertiseData: self.Dict_Beacon_Advertise_Data )
                            
                            if is_from_Notification
                            {
                                self.delegate?.didClickedOnAdvertise?(AdvertiseData: self.Dict_Beacon_Advertise_Data )
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    let state: UIApplication.State = UIApplication.shared.applicationState
                                    
                                    if state != .active
                                    {
                                        if self.is_enable_notification
                                        {
                                            if #available(iOS 10.0, *) {
                                                let content = UNMutableNotificationContent()
                                                content.title = String(format: "%@", self.Dict_Beacon_Advertise_Data["name"] as! String)
                                                
                                                var Str_Description = ""
                                                
                                                if self.CheckNullOrNill(value: self.Dict_Beacon_Advertise_Data["description"] as AnyObject)
                                                {
                                                    Str_Description = String(format: "%@", self.Dict_Beacon_Advertise_Data["description"] as! String)
                                                }
                                                else
                                                {
                                                    Str_Description = ""
                                                }
                                                
                                                if #available(iOS 13.0, *)
                                                {
                                                    content.body = Str_Description
                                                    content.sound = .default
                                                    content.userInfo = ["Beacon_UUID": Beacon_UUID]
                                                }
                                                else
                                                {
                                                    // Fallback on earlier versions
                                                    content.body = Str_Description
                                                    content.sound = .default
                                                    content.userInfo = ["Beacon_UUID": Beacon_UUID]
                                                }
                                                
                                                let request = UNNotificationRequest(identifier: Beacon_UUID, content: content, trigger: nil)
                                                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
                                            }
                                            else
                                            {
                                                // Fallback on earlier versions
                                                let notification = UILocalNotification()
                                                notification.fireDate = NSDate(timeIntervalSinceNow: 0) as Date
                                                notification.alertTitle = String(format: "%@", self.Dict_Beacon_Advertise_Data["name"] as! String)
                                                
                                                
                                                var Str_Description = ""
                                                
                                                if self.CheckNullOrNill(value: self.Dict_Beacon_Advertise_Data["description"] as AnyObject)
                                                {
                                                    Str_Description = String(format: "%@", self.Dict_Beacon_Advertise_Data["description"] as! String)
                                                }
                                                else
                                                {
                                                    Str_Description = ""
                                                }
                                                
                                                notification.alertBody = Str_Description
                                                notification.alertAction = "View"
                                                notification.soundName = UILocalNotificationDefaultSoundName
                                                UIApplication.shared.scheduleLocalNotification(notification)
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            
                            self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: "Somthing went Wrong, try after Sometime")
                        }
                    }
                    else
                    {
                        self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: "Somthing went Wrong, try after Sometime")
                    }
                }
                else
                {
                    self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: "Somthing went Wrong, try after Sometime")
                }
            }
            else
            {
                self.Open_Alert(Title: "QuantumUSB BluetoothBeacon Alert...!!", Message: "Somthing went Wrong, try after Sometime")
            }
        });dataTask.resume()
    }
    
    func CheckNullOrNill(value : AnyObject?) -> Bool
    {
        if value is NSNull
        {
            return false
        }
        else if value == nil
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion)
    {
        let beaconRegion = region as! CLBeaconRegion
        let Str_Beacon_UUID = String(format: "%@", beaconRegion.proximityUUID as CVarArg)
        self.Get_Beacon_Advertise_Data(Beacon_UUID: Str_Beacon_UUID, is_from_Notification: false)
    }
    
    public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion)
    {
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        if #available(iOS 14.0, *) {
            completionHandler([.list, .badge, .sound])
        } else {
            // Fallback on earlier versions
            completionHandler([.badge, .sound])
        }
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        if let userInfo = response.notification.request.content.userInfo as? [String: AnyObject]
        {
            if let Beacon_UUID = userInfo["Beacon_UUID"] as? String
            {
                self.Get_Beacon_Advertise_Data(Beacon_UUID: Beacon_UUID, is_from_Notification: false)
            }
        }
    }
    
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification)
    {

    }
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Void)
    {
        
    }


    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, withResponseInfo responseInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void)
    {
        
    }
    
    
    
    @available(iOS 13.0, *)
    public func locationManager(_ manager: CLLocationManager, didRange beacons: [CLBeacon], satisfying beaconConstraint: CLBeaconIdentityConstraint)
    {
    }
    
    var is_immidiate = false
    
    public func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion)
    {
        if beacons.count > 0
        {
            let nearestBeacon = beacons.first!
            switch nearestBeacon.proximity {
            case .immediate:
                // Display information about the relevant exhibit.
                
                is_immidiate = true
                
                break
            case .near:
                
                if is_immidiate == false
                {
                }
                break
                
            default:
                
                is_immidiate = false
                break
            }
        }
    }
    
    func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController?
    {
        if let nav = base as? UINavigationController
        {
            return getTopViewController(base: nav.visibleViewController)
        }
        else if let tab = base as? UITabBarController, let selected = tab.selectedViewController
        {
            return getTopViewController(base: selected)
        }
        else if let presented = base?.presentedViewController
        {
            return getTopViewController(base: presented)
        }
        return base
    }
}

